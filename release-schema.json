{
  "properties": {
    "secondStage": {
      "title": "Second stage",
      "description": "Data regarding the second stage of a multi-stage procurement process as framework agreements or prequalifications.",
      "$ref": "#/definitions/SecondStage"
    }
  },
  "definitions": {
    "Award": {
      "properties": {
        "buyer": {
          "title": "Buyer",
          "description": "The entity whose budget will be used to purchase the goods or services specified in this particular award.",
          "$ref": "#/definitions/OrganizationReference"
        }
      }
    },
    "Lot": {
      "properties": {
        "simultaneousSupply": {
          "title": "Simultaneous Supply",
          "type": [
            "boolean",
            "null"
          ],
          "description": "Determines if the lot is intended to be awarded as simultaneous supply"
        },
        "statusDetails": {
          "title": "Status Details",
          "description": "The status local name",
          "type": [
            "string",
            "null"
          ]
        },
        "openContractType": {
          "title": "Open Contract Type",
          "description": "If this lot is intented to be awarded as an open contract, the type of open contrat (amount or quantity) should be registered in this field",
          "type": [
            "string",
            "null"
          ]
        },
        "minValue": {
          "title": "Lot minimum value",
          "$ref": "#/definitions/Value",
          "description": "The minimum estimated value of this lot."
        }
      }
    },
    "Invitation": {
      "title": "Invitation",
      "description": "Data regarding an invitation to bid process.",
      "type": "object",
      "required": [
        "id"
      ],
      "properties": {
        "id": {
          "title": "Invitation ID",
          "description": "An identifier for this invitation to bid process.",
          "type": [
            "string",
            "integer"
          ],
          "minLength": 1,
          "versionId": true
        },
        "title": {
          "title": "Invitation title",
          "description": "A title for this invitation. This will often be used by applications as a headline to attract interest, and to help analysts understand the nature of this procurement.",
          "type": [
            "string",
            "null"
          ]
        },
        "description": {
          "title": "Invitation description",
          "description": "A summary description of the invitaiton. This complements any structured information provided using the items array. Descriptions should be short and easy to read. Avoid using ALL CAPS.",
          "type": [
            "string",
            "null"
          ]
        },
        "status": {
          "title": "Tender status",
          "description": "The current status of the tender, from the closed [tenderStatus](https://standard.open-contracting.org/1.1/en/schema/codelists/#tender-status) codelist.",
          "type": [
            "string",
            "null"
          ],
          "codelist": "tenderStatus.csv",
          "openCodelist": false,
          "enum": [
            "planning",
            "planned",
            "active",
            "cancelled",
            "unsuccessful",
            "complete",
            "withdrawn",
            null
          ]
        },
        "procuringEntity": {
          "title": "Procuring entity",
          "description": "The entity managing the procurement. This may be different from the buyer who pays for, or uses, the items being procured.",
          "$ref": "#/definitions/OrganizationReference"
        },
        "lots": {
          "title": "Lots to be procured",
          "description": "Lots to be procured",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Lot"
          },
          "uniqueItems": true
        },
         "criteria": {
          "title": "Criteria",
          "description": "A list of criteria on which either bidders or items will be judged, evaluated or assessed.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Criterion"
          },
          "uniqueItems": true
        },
        "bidOpening": {
          "title": "Bid opening",
          "description": "The date, time, place and other details of the bid opening.",
          "$ref": "#/definitions/BidOpening"
        },
        "enquiriesAddress": {
          "$ref": "#/definitions/Address",
          "description": "Lugar de aclaración del proceso",
          "title": "Lugar de aclaración"
        },
        "items": {
          "title": "Items to be procured",
          "description": "The goods and services to be purchased, broken into line items wherever possible. Items should not be duplicated, but the quantity specified instead.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Item"
          },
          "uniqueItems": true
        },
        "value": {
          "title": "Value",
          "description": "The total upper estimated value of the procurement. A negative value indicates that the contracting process may involve payments from the supplier to the buyer (commonly used in concession contracts).",
          "$ref": "#/definitions/Value"
        },
        "minValue": {
          "title": "Minimum value",
          "description": "The minimum estimated value of the procurement.  A negative value indicates that the contracting process may involve payments from the supplier to the buyer (commonly used in concession contracts).",
          "$ref": "#/definitions/Value"
        },
        "procurementMethod": {
          "title": "Procurement method",
          "description": "The procurement method, from the closed [method](https://standard.open-contracting.org/1.1/en/schema/codelists/#method) codelist.",
          "type": [
            "string",
            "null"
          ],
          "codelist": "method.csv",
          "openCodelist": false,
          "enum": [
            "open",
            "selective",
            "limited",
            "direct",
            null
          ]
        },
        "procurementMethodDetails": {
          "title": "Procurement method details",
          "description": "Additional detail on the procurement method used. This field can be used to provide the local name of the particular procurement method used.",
          "type": [
            "string",
            "null"
          ]
        },
        "procurementMethodRationale": {
          "title": "Procurement method rationale",
          "description": "Rationale for the chosen procurement method. This is especially important to provide a justification in the case of limited tenders or direct awards.",
          "type": [
            "string",
            "null"
          ]
        },
        "mainProcurementCategory": {
          "title": "Main procurement category",
          "description": "The primary category describing the main object of this contracting process, from the closed [procurementCategory](https://standard.open-contracting.org/1.1/en/schema/codelists/#procurement-category) codelist.",
          "type": [
            "string",
            "null"
          ],
          "codelist": "procurementCategory.csv",
          "openCodelist": false,
          "enum": [
            "goods",
            "works",
            "services",
            null
          ]
        },
        "additionalProcurementCategories": {
          "title": "Additional procurement categories",
          "description": "Any additional categories describing the objects of this contracting process, using the open [extendedProcurementCategory](https://standard.open-contracting.org/1.1/en/schema/codelists/#extended-procurement-category) codelist.",
          "type": [
            "array",
            "null"
          ],
          "items": {
            "type": "string"
          },
          "codelist": "extendedProcurementCategory.csv",
          "openCodelist": true
        },
        "awardCriteria": {
          "title": "Award criteria",
          "description": "The award criteria for the procurement, using the open [awardCriteria](https://standard.open-contracting.org/1.1/en/schema/codelists/#award-criteria) codelist.",
          "type": [
            "string",
            "null"
          ],
          "codelist": "awardCriteria.csv",
          "openCodelist": true
        },
        "awardCriteriaDetails": {
          "title": "Award criteria details",
          "description": "Any detailed or further information on the award or selection criteria.",
          "type": [
            "string",
            "null"
          ]
        },
        "submissionMethod": {
          "title": "Submission method",
          "description": "The methods by which bids are submitted, using the open [submissionMethod](https://standard.open-contracting.org/1.1/en/schema/codelists/#submission-method) codelist.",
          "type": [
            "array",
            "null"
          ],
          "items": {
            "type": "string"
          },
          "codelist": "submissionMethod.csv",
          "openCodelist": true
        },
        "submissionMethodDetails": {
          "title": "Submission method details",
          "description": "Any detailed or further information on the submission method. This can include the address, e-mail address or online service to which bids are submitted, and any special requirements to be followed for submissions.",
          "type": [
            "string",
            "null"
          ]
        },
        "submissionPeriod": {
          "title": "Submission period",
          "description": "The period when the invitation is open for submissions. The end date is the closing date for tender submissions.",
          "$ref": "#/definitions/Period"
        },
        "enquiryPeriod": {
          "title": "Enquiry period",
          "description": "The period during which potential bidders may submit questions and requests for clarification to the entity managing procurement. Details of how to submit enquiries should be provided in attached notices, or in submissionMethodDetails. Structured dates for when responses to questions will be made can be provided using tender milestones.",
          "$ref": "#/definitions/Period"
        },
        "hasEnquiries": {
          "title": "Has enquiries?",
          "description": "A true/false field to indicate whether any enquiries were received during the tender process. Structured information on enquiries that were received, and responses to them, can be provided using the enquiries extension.",
          "type": [
            "boolean",
            "null"
          ]
        },
        "eligibilityCriteria": {
          "title": "Eligibility criteria",
          "description": "A description of any eligibility criteria for potential suppliers.",
          "type": [
            "string",
            "null"
          ]
        },
        "awardPeriod": {
          "title": "Evaluation and award period",
          "description": "The period for decision making regarding the contract award. The end date should be the date on which an award decision is due to be finalized. The start date may be used to indicate the start of an evaluation period.",
          "$ref": "#/definitions/Period"
        },
        "contractPeriod": {
          "description": "The period over which the contract is estimated or required to be active. If the tender does not specify explicit dates, the duration field may be used.",
          "title": "Contract period",
          "$ref": "#/definitions/Period"
        },
        "numberOfSubmitters": {
          "title": "Number of submitters",
          "description": "The number of parties who submit a bid.",
          "type": [
            "integer",
            "null"
          ]
        },
        "submitters": {
          "title": "Submitters",
          "description": "All parties who submit a bid on a invitation.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/OrganizationReference"
          },
          "uniqueItems": true
        },
        "documents": {
          "title": "Documents",
          "description": "All documents and attachments related to the tender, including any notices. See the [documentType](https://standard.open-contracting.org/1.1/en/schema/codelists/#document-type) codelist for details of potential documents to include. Common documents include official legal notices of tender, technical specifications, evaluation criteria, and, as a tender process progresses, clarifications and replies to queries.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Document"
          }
        },
        "lots": {
          "title": "Lots",
          "description": "All lots related to this invitation",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Lot"
          }
        },
        "enquiries": {
          "title": "Enquiry",
          "description": "Enquiries made to this invitation",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Enquiry"
          },
          "uniqueItems": true
        },
        "milestones": {
          "title": "Milestones",
          "description": "A list of milestones associated with the tender.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Milestone"
          }
        },
        "amendments": {
          "description": "A tender amendment is a formal change to the tender, and generally involves the publication of a new tender notice/release. The rationale and a description of the changes made can be provided here.",
          "type": "array",
          "title": "Amendments",
          "items": {
            "$ref": "#/definitions/Amendment"
          }
        },
        "notifiedSuppliers": {
          "title": "Notified Suppliers",
          "description": "A list of suppliers that have been invited to bid in this tender. This invitation does not implies that other suppliers cannot make a bid or that the notified ones will do",
          "type": "array",
          "items": {
            "$ref": "#/definitions/OrganizationReference"
          },
          "uniqueItems": true
        }

      }
    },
    "SecondStage": {
      "title": "Second stage",
      "description": "Data regarding the second stage of a multi-stage procurement process as framework agreements or prequalifications.",
      "type": "object",
      "properties": {
        "id": {
          "title": "Second Stage ID",
          "description": "An identifier for this second stage.",
          "type": [
            "string",
            "integer"
          ],
          "minLength": 1,
          "omitWhenMerged": true
        },
        "candidates": {
          "title": "Candidates",
          "description": "The candidates who can submit a bid in an invitation process.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/OrganizationReference"
          },
          "uniqueItems": true
        },
        "invitations": {
          "title": "Invitations",
          "description": "The invitations that are made in the second stage of the procurement process",
          "type": "array",
          "minItems": 1,
          "items": {
            "$ref": "#/definitions/Invitation"
          },
          "uniqueItems": true
        },
        "documents": {
          "title": "Documents",
          "description": "The documents related to the second stage.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/Document"
          },
          "uniqueItems": true
        }
      }
    }
  }
}
